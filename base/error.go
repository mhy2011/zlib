// Author: mahaiyuan
// Date: 2023/7/19 21:37
// Description:

package base

type Error struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

func (err Error) Error() string {
	return err.Msg
}
