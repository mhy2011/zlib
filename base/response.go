// Author: mahaiyuan
// Date: 2023/7/19 21:35
// Description:

package base

import "net/http"
import "github.com/zeromicro/go-zero/rest/httpx"

type Body struct {
	Code int         `json:"code"` //响应码
	Msg  string      `json:"msg"`  //错误提示
	Data interface{} `json:"data"` //返回结果
}

func Response(w http.ResponseWriter, resp interface{}, err error) {

	body := Body{Code: 0, Msg: "OK"}
	if err == nil {
		body.Data = resp
		httpx.OkJson(w, body)
		return
	}

	body.Code = -1
	body.Msg = err.Error()
	switch err.(type) {
	case Error:
		body.Code = err.(Error).Code
		body.Msg = err.(Error).Msg
	default:
	}
	httpx.OkJson(w, body)
}
